import os
import pytest
import tempfile
from unittest.mock import patch, MagicMock
from ai_arduino_code_builder.ArduinoLoader import ArduinoLoader


@pytest.fixture
def mock_serial_port():
    """
    A fixture that mocks the serial port for testing purposes.
    """
    with patch("ai_arduino_code_builder.ArduinoLoader.list_ports.comports") as mock:
        yield mock


@pytest.fixture
def mock_subprocess():
    """
    Fixture for mocking subprocess run with ArduinoLoader, returning success
    """
    with patch("ai_arduino_code_builder.ArduinoLoader.subprocess.run") as mock:
        mock.return_value.stdout = "Success"
        mock.return_value.stderr = ""
        mock.return_value.returncode = 0
        yield mock


class TestArduinoLoader:
    """
    Test suite for the ArduinoLoader class.
    """

    def test_initialization_success(self, mock_serial_port):
        """
        Mock serial port to return a specific device
        """
        # Mock serial port to return a specific device
        mock_serial_port.return_value = [
            MagicMock(vid=0x2341, pid=0x003D, device="/dev/ttyUSB0")
        ]
        loader = ArduinoLoader(
            vid=0x2341, pid=0x003D, board_name="arduino:sam:arduino_due_x"
        )
        assert loader.vid == 0x2341
        assert loader.pid == 0x003D

    def test_get_port_by_vid_pid_found(self, mock_serial_port):
        """
        Test getting port by VID and PID when the port is found.
        """
        mock_serial_port.return_value = [
            MagicMock(vid=0x2341, pid=0x003D, device="/dev/ttyUSB0")
        ]
        port = ArduinoLoader.get_port_by_vid_pid(0x2341, 0x003D)
        assert port == "/dev/ttyUSB0"

    def test_get_port_by_vid_pid_not_found(self, mock_serial_port):
        """
        Test the get_port_by_vid_pid method when the port is not found.
        """
        mock_serial_port.return_value = []
        port = ArduinoLoader.get_port_by_vid_pid(0x2341, 0x003D)
        assert port is None

    def test_run_arduino_upload_success(self, mock_serial_port, mock_subprocess):
        """
        Test the successful upload functionality of running Arduino.
        """
        mock_serial_port.return_value = [
            MagicMock(vid=0x2341, pid=0x003D, device="/dev/ttyUSB0")
        ]
        loader = ArduinoLoader(
            vid=0x2341, pid=0x003D, board_name="arduino:sam:arduino_due_x"
        )
        loader.run_arduino(file_name="analog_in_out.ino", command_type="upload")
        mock_subprocess.assert_called_once()

    def test_run_arduino_verify_success(self, mock_serial_port, mock_subprocess):
        """
        Test the run_arduino_verify_success function with mock serial port and subprocess.
        """
        mock_serial_port.return_value = [
            MagicMock(vid=0x2341, pid=0x003D, device="/dev/ttyUSB0")
        ]
        loader = ArduinoLoader(
            vid=0x2341, pid=0x003D, board_name="arduino:sam:arduino_due_x"
        )
        loader.run_arduino(file_name="analog_in_out.ino", command_type="verify")
        mock_subprocess.assert_called_once()

    # Skip if environment variable "CI" is set
    @pytest.mark.skipif(
        "CI" in os.environ,
        reason="Skipping test that requires an Arduino device when running in CI.",
    )
    def test_load_real_file(self):
        """
        Tests loading a real Arduino file.
        """
        ARDUINO_UNO_VID = 0x2A03
        ARDUINO_UNO_PID = 0x0043

        ARDUINO_SAMPLE_PROGRAM = """
// LED_Blink.ino

void setup() {
// initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(1000);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(1000);                       // wait for a second
}
        """

        # Get the current directory
        current_directory = os.getcwd()

        # Create a temp file in the current directory with a real Arduino file
        with tempfile.NamedTemporaryFile(
            mode="w", delete=False, suffix=".ino", dir=current_directory
        ) as tmp_file:
            tmp_file.write(ARDUINO_SAMPLE_PROGRAM)
            tmp_file.flush()
            tmp_file.close()
            loader = ArduinoLoader(
                vid=ARDUINO_UNO_VID, pid=ARDUINO_UNO_PID, board_name="arduino:avr:uno"
            )
            loader.run_arduino(file_name=tmp_file.name, command_type="upload")
            os.remove(tmp_file.name)
