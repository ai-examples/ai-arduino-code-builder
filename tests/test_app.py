import pytest
from fastapi.testclient import TestClient
from unittest.mock import patch, MagicMock
from ai_arduino_code_builder.app import app  # Ensure correct import path

@pytest.fixture
def client():
    with TestClient(app) as c:
        yield c

@pytest.fixture
def arduino_mock(monkeypatch):
    mock = MagicMock()
    # Set up a default mock behavior that can be overridden in specific tests
    mock.run_arduino.return_value = (0, 'Mocked standard output', '')
    monkeypatch.setattr("ai_arduino_code_builder.app.arduino.run_arduino", mock.run_arduino)
    return mock

class TestApp:
    def test_read_root(self, client):
        response = client.get("/")
        assert response.status_code == 200
        assert response.json() == {"status": "OK"}

    def test_verify_code_success(self, client, arduino_mock):
        snippet = {
            "language": "Python",
            "code": "print('Hello, World!')",
            "description": "A simple print statement"
        }
        response = client.post("/verifyCode", json=snippet)
        assert response.status_code == 200
        # Correcting the assertion to correctly handle the expected response structure
        assert response.json().get("message") == "Verification successful.", "The response structure does not match the expected format."
