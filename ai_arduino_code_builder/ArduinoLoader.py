import subprocess
from serial import Serial
from serial.tools import list_ports


class ArduinoLoader:
    """
    A class to compile and load an Arduino device with a specified INO file.

    Attributes:
        vid (int): Vendor ID of the Arduino device.
        pid (int): Product ID of the Arduino device.
        board_name (str): The name of the Arduino board (e.g., "arduino:sam:arduino_due_x").
        file_name (str): The name of the INO file to upload to the Arduino device.
    """

    def __init__(self, vid, pid, board_name):
        """
        Initializes the ArduinoLoader with specific device and file parameters.

        Parameters:
            vid (int): Vendor ID of the Arduino device.
            pid (int): Product ID of the Arduino device.
            board_name (str): The name of the Arduino board.
            file_name (str): The name of the INO file to upload.
        """
        self.board_name = board_name
        self.vid = vid
        self.pid = pid

    @staticmethod
    def get_port_by_vid_pid(vid, pid):
        """
        A function to retrieve the port based on the provided VID and PID.

        :param vid: The VID (Vendor ID) to search for.
        :param pid: The PID (Product ID) to search for.
        :return: The device associated with the provided VID and PID if found, otherwise None.
        """
        for port in list_ports.comports():
            if port.vid == vid and port.pid == pid:
                return port.device
        return None

    def run_arduino(self, file_name: str, command_type: str = "upload"):
        """
        Construct and display the command to compile and upload the INO file.

        :param command_type: str, the type of command to execute (default is "upload"). Valid values are "verify" and "upload".
        :return: None
        """
        # Construct and display the command to compile and upload the INO file
        command = [
            "arduino",
            "--verbose",
            "--board",
            self.board_name,
            f"--{command_type}",
            file_name,
        ]

        # If command_type is upload then append "--port" and self.target_port to command
        if command_type == "upload":
            # Get the port of the Arduino device
            self.target_port = self.get_port_by_vid_pid(self.vid, self.pid)
            assert (
                self.target_port is not None
            ), "Cannot find an Arduino connected with the specified VID and PID."
            command.extend(["--port", self.target_port])

        print("Running command:", " ".join(command))

        # Execute the command
        result = subprocess.run(
            command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True
        )

        # Return the return code, stdout, and stderr
        return (result.returncode, result.stdout, result.stderr)

    def read_line_from_serial(self, baudrate: int = 9600, timeout: int = 1):
        """
        Read a line from the serial port with the specified baudrate and timeout.

        Parameters:
            baudrate (int): The baudrate for the serial communication. Default is 9600.
            timeout (int): The timeout for reading from the serial port. Default is 1.

        Returns:
            str: The decoded line read from the serial port with trailing whitespaces removed.
        """
        with Serial(self.target_port, baudrate=baudrate) as ser:
            return ser.readline().decode("utf-8").rstrip()
