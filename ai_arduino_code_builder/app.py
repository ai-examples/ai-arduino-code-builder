import logging
import os
import tempfile
from pydantic import BaseModel
from fastapi import FastAPI, Request, HTTPException
from starlette.status import HTTP_422_UNPROCESSABLE_ENTITY
from ai_arduino_code_builder.ArduinoLoader import ArduinoLoader


# Define the data model for the request body
class CodeSnippet(BaseModel):
    language: str
    code: str
    description: str = None  # Optional field


# Configure your logger
logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(name)s - %(levelname)s - %(message)s"
)
logger = logging.getLogger(__name__)

ARDUINO_UNO_VID = 0x2A03
ARDUINO_UNO_PID = 0x0043
ARDUINO_UNO_BOARD_NAME = "arduino:avr:uno"

app = FastAPI()
arduino = ArduinoLoader(
    vid=ARDUINO_UNO_VID, pid=ARDUINO_UNO_PID, board_name=ARDUINO_UNO_BOARD_NAME
)


async def validate_and_process_payload(code: str, command_type: str):
    # Define a filename in the current working directory
    file_name = os.path.join(os.getcwd(), "sketch.ino")

    try:
        # Write the payload to the file
        with open(file_name, "w") as file:
            file.write(code)

        # Validate that the payload is not empty. If it is then raise an HTTP exception
        if not code.strip():
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail="Code cannot be empty",
            )

        # Run the Arduino command
        result_code, stdout, stderr = arduino.run_arduino(
            file_name=file_name, command_type=command_type
        )

        if result_code != 0:
            logger.error(
                f"Arduino command failed with error: {stderr} (Return code: {result_code})"
            )
            logger.info(f"Code payload: {code}")
            raise HTTPException(
                status_code=HTTP_422_UNPROCESSABLE_ENTITY,
                detail=f"Arduino command failed with error: {stderr} (Return code: {result_code})",
            )

        return stdout
    finally:
        # Ensure the file is removed after processing
        if os.path.exists(file_name):
            os.remove(file_name)


@app.get("/")
async def read_root():
    return {"status": "OK"}


@app.post("/verifyCode")
async def verify_code(snippet: CodeSnippet):
    logging.info(
        f"Received a verify request with the following code snippet: Language={snippet.language}, Description={snippet.description}, Code=\n{snippet.code}"
    )
    try:
        output = await validate_and_process_payload(
            code=snippet.code, command_type="verify"
        )
        return {"message": "Verification successful.", "output": output}
    except HTTPException as e:
        return {"error": e.detail}, e.status_code


@app.post("/uploadCode")
async def upload_code(snippet: CodeSnippet):
    logging.info(
        f"Received an upload request with the following code snippet: Language={snippet.language}, Description={snippet.description}, Code=\n{snippet.code}"
    )
    try:
        output = await validate_and_process_payload(
            code=snippet.code, command_type="upload"
        )
        return {"message": "Upload successful.", "output": output}
    except HTTPException as e:
        return {"error": e.detail}, e.status_code


@app.get("/readSerial")
async def read_line_from_serial(baudrate: int = 9600, timeout: int = 1):
    try:
        # Attempt to read a line from the serial port using the specified baudrate and timeout
        line = arduino.read_line_from_serial(baudrate=baudrate, timeout=timeout)
        return {"message": "Success", "data": line}
    except Exception as e:
        logger.error(f"Failed to read from serial: {str(e)}")
        raise HTTPException(
            status_code=HTTP_422_UNPROCESSABLE_ENTITY,
            detail="Failed to read from the serial port.",
        )
