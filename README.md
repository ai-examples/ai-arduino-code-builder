# AI Arduino Code Builder
Leveraging AI to write code for an Arduino and then execute it via GPT Actions

## Overview
Leveraging the concepts from the [GPT Actions Server](https://gitlab.com/ai-examples/gpt-actions-webserver) this project creates an endpoint to a physical Arduino Uno for GPT Actions to write code to and execute directly on the hardware target.

| Component        | Description                                                                                                           |
|------------------|-----------------------------------------------------------------------------------------------------------------------|
| Web Applications | Python-based web applications serve as an API for controlling the Arduino, allowing external services like ChatGPT to interface with lab hardware. |
| Nginx            | A web server for SSL/TLS management, routing HTTPS traffic to the correct web applications, and serving the `openapi.yaml` file for API documentation. |
| Docker Compose   | Manages multi-container Docker applications, linking the web application and Nginx to simplify network configuration and deployment. |
| openapi.yaml     | The OpenAPI Schema detailing the API for GPT Actions, facilitating the interpretation of commands and data exchange with the server. |
| SSL Certificate  | Required for HTTPS setup. These private files (e.g., keys and certificates) are essential for secure communication but are excluded from the repository to maintain security. |

## How it Works
This service utilizes a Python-based web application to facilitate direct interaction between GPT Actions and an Arduino Uno device. Through the web application, GPT Actions can dynamically write and execute Arduino code, enabling a wide range of physical interactions and automations. Here's a brief overview of the process:

1. **Request Handling**: The web application receives requests from external services (e.g., ChatGPT) via the API described in `openapi.yaml`.
1. **Code Generation and Execution**: Utilizing AI models, the service generates Arduino code based on the received commands and uploads it to the connected Arduino Uno for execution.
1. **Hardware Interaction**: The executed code interacts with the Arduino hardware, allowing for real-world actions based on AI-generated instructions.

## Prerequisites
1. Docker
1. Opening of the firewall to your computer where this code is hosted (ports 80 and 443)
1. SSL Certificate
1. Paid version of ChatGPT
1. Replacing `amahpour.servehttp.com` with your domain name

## How to Run
In the root of the repository run the following command:
```
docker compose up
```
Then configure your GPT Action by passing in the URL to this server and click on "Import from URL."

### Example Prompts
Here are some example prompts you can pass into ChatGPT after setting up your GPT Action:

#### Blinky LED
Write Arduino Uno code that blinks the onboard LED at a rate of 250 ms. Validate it and upload it to my board.

#### Serial Message
Write Arduino Uno code that prints out, over serial, a message with a count up every 1 second. For example:
```
Hello. This is iteration 1
Hello. This is iteration 2
Hello. This is iteration 3
```
Validate it and upload it to my board. Then read from the serial port a 3 times and report it back to me.

## Development
### Building the Container
The Docker container can be used on multiple platforms. As such it needs to be built for multiple architectures using the following commands:
```
docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx create --use
docker buildx build --platform linux/arm/v7,linux/amd64 -t ai-arduino-code-builder .
```