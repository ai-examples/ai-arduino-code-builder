# Base stage for common setup
FROM registry.gitlab.com/docker-embedded/arduino-ide-on-raspberry-pi-4

# Set the working directory in the container
WORKDIR /app

# Install necessary packages within container
RUN apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    python-is-python3 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install any needed packages specified in requirements.txt
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt --break-system-packages
RUN rm requirements.txt

# Copy the contents within ai-arduino-code-builder into /app
COPY ai_arduino_code_builder/ /app/ai_arduino_code_builder/

# Command to run the DL3021 app
CMD ["uvicorn", "ai_arduino_code_builder.app:app", "--host", "0.0.0.0", "--port", "8000"]